/*
 * memalloclib.c
 *
 *  Created on: 11 ���. 2019 �.
 *      Author: ������
 */

#include <stdlib.h>
#include <stdint.h>
#include <windows.h>
#include "memalloclib.h"

// ���������-��������� ������� ��� ������������� ��������
#define PlatformMutexType			HANDLE
#define PlatformMutexCreate()		CreateMutex(NULL, FALSE, NULL)
#define PlatformMutexTake(m)		WaitForSingleObject(m, INFINITE)
#define PlatformMutexGive(m)		ReleaseMutex(m)

// ��� ��������� ��� �������� � ������ ����� � ����� ��� ���������
typedef struct
{
	uint8_t busy;
	uint8_t body[MEM_BLOCK_SIZE];
} tMemBlock;

static tMemBlock MemBlocks[MEM_BLOCK_NUM];		// ��� ������
static PlatformMutexType Mux = 0;				// ����� ������� ������������� ������� � ����� ��������� ������ ������
static uint32_t FreeBlockNum = MEM_BLOCK_NUM;	// ������� ���-�� ��������� ������
static uint32_t FirstFreeBlockIdx = 0;			// ������ ������� ���������� ����� � ���� (��� ����������� ������ ��������� ������)

//----------------------------------------------------------------------------------
// ������� ���������� ��������� �� ������ �� �������� ����� ������, ����� ��� ��� �������.
// ���������� 0 � ������ ������ ��� ���� ��������� ����� ���������
//----------------------------------------------------------------------------------
void *allocBlock(void)
{
	if (!Mux)
		Mux = PlatformMutexCreate();
	if (!Mux)
		return 0;

	PlatformMutexTake(Mux);

	// ���� ������ ���������� ����� ������
	for (uint32_t blockIdx = FirstFreeBlockIdx; blockIdx < MEM_BLOCK_NUM; blockIdx++)
	{
		if (!MemBlocks[blockIdx].busy)		// ������ ��������� ���� ������
		{
			MemBlocks[blockIdx].busy = 1;

			if (blockIdx + 1 < MEM_BLOCK_NUM)
				FirstFreeBlockIdx = blockIdx + 1;

			FreeBlockNum--;
			PlatformMutexGive(Mux);
			return &MemBlocks[blockIdx].body;
		}
	}

	PlatformMutexGive(Mux);
	return 0;
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// ��������� ����������� ���� ������, ��������� �� ������� �� ������� � �������� pMem
//----------------------------------------------------------------------------------
void freeBlock(void *pMem)
{
	if (!Mux)
		Mux = PlatformMutexCreate();
	if (!Mux)
		return;

	for (uint32_t blockIdx = 0; blockIdx < MEM_BLOCK_NUM; blockIdx++)
	{
		if (&MemBlocks[blockIdx].body == pMem)
		{
			PlatformMutexTake(Mux);
			MemBlocks[blockIdx].busy = 0;

			// ���� ���� �������������, ����������� ������ ���������� ����� ����� �������� � ����
			if (blockIdx < FirstFreeBlockIdx)
				FirstFreeBlockIdx = blockIdx;

			FreeBlockNum++;
			PlatformMutexGive(Mux);
			return;
		}
	}
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// ��������� ����������� ��� ����� ������
//----------------------------------------------------------------------------------
void freeAllBlocks(void)
{
	if (!Mux)
		Mux = PlatformMutexCreate();

	PlatformMutexTake(Mux);

	for (uint32_t blockIdx = 0; blockIdx < MEM_BLOCK_NUM; blockIdx++)
		MemBlocks[blockIdx].busy = 0;

	PlatformMutexGive(Mux);
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// ������� ���������� ���-�� ��������� ������ ������
//----------------------------------------------------------------------------------
uint32_t getFreeBlockNum(void)
{
	return FreeBlockNum;
}
//----------------------------------------------------------------------------------

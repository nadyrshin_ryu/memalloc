/*
 * memalloclib.h
 *
 *  Created on: 11 ���. 2019 �.
 *      Author: ������
 */

#ifndef MEMALLOCLIB_H_
#define MEMALLOCLIB_H_


// ������ 1 ����� � ���-�� ������ � ����
#define MEM_BLOCK_SIZE		256
#define MEM_BLOCK_NUM		100000



// ������� ���������� ��������� �� ������ �� �������� ����� ������, ����� ��� ��� �������.
// ���������� 0 � ������ ������ ��� ���� ��������� ����� ���������
void *allocBlock(void);
// ��������� ����������� ���� ������, ��������� �� ������� �� ������� � �������� pMem
void freeBlock(void *pMem);
// ��������� ����������� ��� ����� ������
void freeAllBlocks(void);
// ������� ���������� ���-�� ��������� ������ ������
uint32_t getFreeBlockNum(void);


#endif /* MEMALLOCLIB_H_ */

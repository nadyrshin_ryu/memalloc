/*
 ============================================================================
 Name        : DemoApp.c
 Author      : Ruslan Nadyrshin
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <process.h>
#include <windows.h>
#include "memalloclib.h"


// ��� ��������� ��� �������� ���������� � ����� ������ ��������������� �����
typedef struct
{
    uint8_t threadNum;
    uint32_t blockNum;
} tTestThreadArgs;


#define MULTITEST_THREAD_NUM		5		// ���-�� ������� ��� ��������������� �����
static int MultithreadTestResults[MULTITEST_THREAD_NUM];	// ���-�� ���������� ������ � ������ �� �������� �������


//----------------------------------------------------------------------------------
// ������� �������� ����� ���������/������������ ������ ������
//----------------------------------------------------------------------------------
int BasicTest(void)
{
	// ������� ������ ����������
	freeAllBlocks();

	printf("Basic test started\r\n");

	uint8_t *pBlocks[10];

	uint32_t freeBlocksBefore = getFreeBlockNum();

	// ��������� 10 ������
	for (int blockCnt = 0; blockCnt < 10; blockCnt++)
	{
		pBlocks[blockCnt] = allocBlock();
		if (!pBlocks[blockCnt])
		{
			printf("Error allocating block #%d\r\n", blockCnt + 1);
			return -1;
		}
	}

	// ������������ 10 ������
	for (int blockCnt = 0; blockCnt < 10; blockCnt++)
		freeBlock(pBlocks[blockCnt]);

	// ���� ��������� ����������� ���� ���������� ��������� ������ �� ��������� � ������������ 10 ������
	// � 1-�������� ������ ����� ���-�� ��������� ������ �����
	uint32_t freeBlocksAfter = getFreeBlockNum();
	if (freeBlocksBefore == freeBlocksAfter)
	{
		printf("Basic test finished OK\r\n");
		return 0;
	}
	else
	{
		printf("Basic test filed, free blocks before test = %d, free blocks after test = %d\r\n", freeBlocksBefore, freeBlocksAfter);
		return -1;
	}
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// ������� ������ � �������������� �����
//----------------------------------------------------------------------------------
unsigned __stdcall  getBlocksThread(void *threadArgs)
{
	tTestThreadArgs *args = (tTestThreadArgs*)threadArgs;
	int blockNum = args->blockNum;
	int allocOk = 0;
	int allocErr = 0;

	// ��������� � ������������ 1 �����. �� ������ �������� �� �������� ���-�� ���������� ������
	uint8_t *pBlock = allocBlock();
	if (pBlock)
		freeBlock(pBlock);

	// ��������� ��� blockNum ������ ��� ������������
	while (blockNum--)
	{
		if (allocBlock())
			allocOk++;
		else
			allocErr++;
	}

	MultithreadTestResults[args->threadNum] = allocOk;
	printf("Thread %d, blocks allocated: %d, blocks not allocated: %d\r\n", args->threadNum + 1, allocOk, allocErr);

	_endthreadex(0);
	return 0;
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// ���� ������������ ����������� ������ � ������� ������
//----------------------------------------------------------------------------------
int MultithreadingTest(void)
{
	// ������� ������ ����������
	freeAllBlocks();

	printf("Multithreading test started\r\n");

	tTestThreadArgs args[MULTITEST_THREAD_NUM];			// ��������� ��� �������� ���������� � ������
	HANDLE ThreadHandles[MULTITEST_THREAD_NUM];			// ������ �������

	for (int threadN = 0; threadN < MULTITEST_THREAD_NUM; threadN++)
	{
		// ���������� ���������� ��� ������ ������
		args[threadN].threadNum = threadN;				// ������ ������ (� 0)
		args[threadN].blockNum = MEM_BLOCK_NUM * 2 / MULTITEST_THREAD_NUM;		// ���������� ������ (�������� ������ �������� ������ ������ ��� ���� � ����)

		HANDLE result = (HANDLE)_beginthreadex(NULL, 2048, getBlocksThread, &args[threadN], 0, NULL);
		if (result <= 0)	// ������ ������ ������
		{
			printf("Error starting thread %d\r\n", args->threadNum);
			return -1;
		}
		ThreadHandles[threadN] = result;
	}

	// �������� ���������� ���� �������
	WaitForMultipleObjectsEx(MULTITEST_THREAD_NUM, ThreadHandles, TRUE, INFINITE, FALSE);

	// ������� ���-�� ���������� ������
	int SumAllocated = 0;
	for (int threadN = 0; threadN < MULTITEST_THREAD_NUM; threadN++)
		SumAllocated += MultithreadTestResults[threadN];

	// ���� ��������� ����������� ���� ����� ���������� ������ ����� ���-�� ������ � ����
	if (SumAllocated == MEM_BLOCK_NUM)
	{
		printf("Multithreading test finished OK\r\n");
		return 0;
	}
	else
	{
		printf("Multithreading test filed, allocated %d blocks from %d\r\n", SumAllocated, MEM_BLOCK_NUM);
		return -1;
	}
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
int main(void)
{
	if (BasicTest() < 0)
	{
		printf("Tests filed");
		return 0;
	}

	if (MultithreadingTest() < 0)
	{
		printf("Tests filed");
		return 0;
	}

	printf("All tests finished successfully");
	return 0;
}
//----------------------------------------------------------------------------------
